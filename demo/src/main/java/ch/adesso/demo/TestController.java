package ch.adesso.demo;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @GetMapping("/test")
    public long latestDefinitions() {
        runtimeService.startProcessInstanceByKey("oneTaskProcess");
        return taskService.createTaskQuery().count();
    }
}
